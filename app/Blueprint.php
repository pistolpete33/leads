<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blueprint extends Model
{
    protected $fillable = [
        'uuid',
        'event_uuid',
        'user_uuid',
        'name',
        'is_active'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'uuid';
    }

    public function versions()
    {
        return $this->hasMany(BlueprintVersion::class, 'blueprint_uuid', 'uuid');
    }

    public function event()
    {
        return $this->belongsTo(BlueprintEvent::class, 'event_uuid', 'uuid');
    }
}
