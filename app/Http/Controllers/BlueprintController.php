<?php

namespace App\Http\Controllers;

use App\Blueprint;
use App\Http\Resources\BlueprintResource;

class BlueprintController extends Controller
{
    public function index()
    {
        return BlueprintResource::collection(Blueprint::paginate());
    }

    public function show(Blueprint $blueprint)
    {
        return new BlueprintResource($blueprint);
    }
}
