<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

/**
 * Class VueController
 * @package App\Http\Controllers
 */
class VueController extends Controller
{
    /**
     * @return View
     */
    public function index(): View
    {
        return view('vue');
    }
}
