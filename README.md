<<<<<<< HEAD
<p align="center"><img src="https://www.booj.com/uploads/main-510c0fcd5e8a0.jpg"></p>

## Booj Lead Routing Service

This web application houses the lead routing service for Booj. 
- Backend API built on [Laravel 6.0](https://laravel.com)
- API documented with a [Postman](https://postman.com) collection
- Deployed using [Laravel Vapor](https://vapor.laravel.com/) and AWS Lambda
- MySQL database with factories and seeders
- JWT authentication using [Laravel Passport](https://laravel.com/docs/6.x/passport)
- [Vue.js](https://vuejs.org/) single page application on the front end
- [Vuetify 2.0](https://vuetifyjs.com/) for rapid and simple styling
- Integrated with [JointJS](https://www.jointjs.com/) for lead routing diagrams

### Important Links:

- [Live site served using Laravel Vapor](https://calm-sun-axaarb36fl2t.vapor-farm-b1.com)
- [Current Postman Collection](https://documenter.getpostman.com/view/4437090/SWTG5vKU?version=latest)

## Local Installation Instructions
###Valet Installation (MacOS only)
1. Clone the git repository to your local machine
1. Install dependencies globally (PHP 7.2 or greater, Composer, Valet, Node/NPM, MySQL)
1. Run Valet install globally 
1. Navigate to the parent of the project directory in terminal
1. Run Valet park to serve the sites in this directory or Valet link to link a specific site to serve
1. Navigate into the project directory 
1. Run `composer install` to install PHP packages 
1. Run `npm install` to install Node packages
1. Place your local MySQL credentials in the .env file 
1. Ensure that a database exists in your local MySQL and that it is specified in the .env
1. Run `php artisan key:generate` to create an application key
1. Run `php artisan migrate --seed` to migrate and seed the database
1. Run `php artisan passport:install` to install Passport
1. Place the password client key and key id (ID 2) into your .env
1. Application should now be running at {{project directory}}.test
