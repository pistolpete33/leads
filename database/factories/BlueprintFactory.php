<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blueprint;
use Faker\Generator as Faker;
use \Illuminate\Support\Str;

$factory->define(Blueprint::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'uuid' => Str::uuid(),
    ];
});
