<?php

use Illuminate\Database\Seeder;
use App\Blueprint;

class BlueprintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Blueprint::class, 1)->create([
            'name' => 'Incoming Form Blueprint',
            'event_id' => 1
        ]);
    }
}
