<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** Application Routes **/
Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'auth:api'], function () {
        Route::apiResource('blueprints', 'BlueprintController')->only(['index', 'show']);
        Route::post('/logout', 'AuthController@logout');
    });

    /** Authentication Routes **/
    Route::post('/login', 'AuthController@login');
});