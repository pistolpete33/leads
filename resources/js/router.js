import Lists from "./components/Lists/Lists";
import List from "./components/Lists/List";
import Books from "./components/Books/Books";
import Book from "./components/Books/Book";
import Home from "./components/Application/Home";
import Login from "./components/Auth/Login";
import Logout from "./components/Auth/Logout";
import ListForm from "./components/Lists/ListForm";
import Team from "./components/Team";
import Blueprints from "./components/Application/Blueprints/Blueprints";
import Blueprint from "./components/Application/Blueprints/Blueprint";
import Events from "./components/Application/Events";
import Leads from "./components/Application/Leads";
import Nodes from "./components/Application/Nodes";

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/team',
        name: 'team',
        component: Team
    },
    {
        path: '/blueprints',
        name: 'blueprints',
        component: Blueprints,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/events',
        name: 'events',
        component: Events,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/leads',
        name: 'leads',
        component: Leads,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/nodes',
        name: 'nodes',
        component: Nodes,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
    },
    {
        path: '/logout',
        name: 'logout',
        component: Logout,
    },
    {
        path: '/blueprints/:uuid',
        name: 'blueprints.show',
        component: Blueprint,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/books',
        name: 'books.index',
        component: Books,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/lists/new',
        name: 'lists.make',
        component: ListForm,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/lists:id',
        name: 'lists.show',
        component: List,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/lists',
        name: 'lists.index',
        component: Lists,
        meta: {
            requiresAuth: true,
        },
    },

];
