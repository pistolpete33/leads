import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);
axios.defaults.baseURL = process.env.MIX_APP_URL + '/api/v1/';

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        pagination: {},
        blueprints: [],
        blueprint: {
            id: '',
            uuid: '',
            event_id: '',
            name: '',
            is_active: '',
            created_at: '',
            updated_at: '',
        },
        headers: [],
        activeTab: 0,
    },
    getters: {
        getToken(state) {
            return state.token;
        },
        loggedIn(state) {
            return state.token != null;
        },
        getBlueprints(state) {
            return state.blueprints;
        },
        getBlueprint(state) {
            return state.blueprint;
        },
        getHeaders(state) {
            return state.headers;
        },
        getActiveTab(state) {
            return state.activeTab;
        }
    },
    mutations: {
        setToken(state, token) {
            state.token = token
        },
        destroyToken(state) {
            state.token = null;
        },
        makePagination(state, meta, links) {
            state.pagination = {
                meta: meta,
                links: links
            }
        },
        setBlueprints(state, blueprints) {
            state.blueprints = blueprints;
        },
        setBlueprint(state, blueprint) {
            state.blueprint = blueprint;
        },
        setHeaders(state, sourceObject) {
            state.headers = Object.keys(sourceObject).map(header => {
                return {'text': header, 'value': header}
            });
        },
        setActiveTab(state, tab) {
            state.activeTab = tab;
        }
    },
    actions: {
        fetchToken(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post('login', {
                    username: credentials.username,
                    password: credentials.password
                }).then(response => {
                    const token = response.data.access_token;
                    if (token !== 'undefined') {
                        localStorage.setItem('access_token', token);
                        context.commit('setToken', token);
                    }
                    resolve(response);
                }).then(error => {
                    console.log(error);
                    reject(error);
                })
            })
        },
        destroyToken(context) {
            if (context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
                    axios.post('logout').then(response => {
                        localStorage.removeItem('access_token');
                        context.commit('destroyToken');
                        resolve(response);
                    }).then(error => {
                        localStorage.removeItem('access_token');
                        context.commit('destroyToken');
                        console.log(error);
                        reject(error);
                    })
                })
            }
        },
        fetchBlueprints(context) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            axios.get('blueprints')
                .then(response => {
                    context.commit('setBlueprints', response.data.data);
                    context.commit('setHeaders', response.data.data[0]);
                    context.commit('makePagination', response.data.meta, response.data.links);
                })
                .catch(error => {
                    console.log(error);
                })
        },
        fetchBlueprint(context, uuid) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token;
            axios.get('blueprints/' + uuid)
                .then(response => {
                    context.commit('setBlueprint', response.data.data);
                })
                .catch(error => {
                    console.log(error);
                })
        },
    }
});
